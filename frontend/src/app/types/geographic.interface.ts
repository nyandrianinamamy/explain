export interface Territory {
    id: string;
    name: string;
    code: string;
}

/**
 * Node for geographic item
 */
export class GeographicNode {
    children!: GeographicNode[];
    item!: Territory;
}

/** Flat geographic item node with expandable and level information */
export class GeographicFlatNode {
    item!: Territory;
    level!: number;
    expandable!: boolean;
}

/** Flat node with expandable and level information */
export class DynamicFlatNode {
    constructor(public item: Territory, public level = 1, public expandable = false,
        public isLoading = false) { }
}