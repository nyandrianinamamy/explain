import { FlatTreeControl } from '@angular/cdk/tree';
import { Component, Input } from '@angular/core';
import { DynamicDataSource } from 'src/app/search-page/dynamic.data-source';
import { DynamicFlatNode } from 'src/app/types/geographic.interface';

@Component({
  selector: 'app-geographic-filter',
  templateUrl: 'geographic-filter.component.html',
  styleUrls: ['geographic-filter.component.scss']
})
export class GeographicFilterComponent {

  @Input() dataSource!: DynamicDataSource;
  @Input() treeControl!: FlatTreeControl<DynamicFlatNode>

  constructor() {

  }

  hasChild = (_: number, _nodeData: DynamicFlatNode) => _nodeData.expandable;
}