import { Component, Input, OnInit } from '@angular/core';
import { Elected } from '../../types/elected.interface';

@Component({
  selector: 'app-elected-card',
  templateUrl: './elected-card.component.html',
  styleUrls: ['./elected-card.component.scss']
})
export class ElectedCardComponent implements OnInit {
  @Input() item!: Elected;

  constructor() {
  }

  ngOnInit(): void {
  }

}
