import { Component, Input, OnInit } from '@angular/core';
import { Elected } from '../../types/elected.interface';

@Component({
  selector: 'app-elected-list',
  templateUrl: './elected-list.component.html',
  styleUrls: ['./elected-list.component.scss']
})
export class ElectedListComponent implements OnInit {
  @Input() items!: Elected[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
