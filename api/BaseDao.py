import csv
from utils import *


class BaseDao:
    # Actual data holder
    data = []

    # Indexes are used to achieve O(1) time complexity
    # On retrieving single item from the array
    index_id_code = {}
    index_code_idData = {}

    parents = []

    def __init__(self) -> None:
        self.data = []
        self.index_id_code = {}
        self.index_code_idData = {}
        self.parents = []

    # Load data from csv to in memory array
    def load_territories(self, kind: Kind):
        with open("data/territories.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            next(reader)
            i = 0
            for row in reader:
                if row[KIND] == kind.value:
                    self.data.append({
                        "id": row[ID],
                        "name": row[NAME],
                        "code": row[CODE]
                    })

                    # Populating indexes
                    self.index_id_code[row[ID]] = row[CODE]
                    self.index_code_idData[row[CODE]] = i
                    i += 1

    # Load parent-child from csv to in memory array
    def load_territory_parent(self):
        with open("data/territory_parents.csv") as csvfile:
            reader = csv.reader(csvfile, delimiter=',')
            next(reader)

            for row in reader:
                self.parents.append({
                    "parent_id": row[PARENT_ID],
                    "child_id": row[CHILD_ID]
                })

    def get_instance(self):
        return self.instance

    def get_lists(self):
        return self.data

    def get_item(self, code: str):
        if code not in self.index_code_idData:
            return None
        index = self.index_code_idData[code]
        return self.data[index]

    def get_item_by_id(self, id: str):
        if id not in self.index_id_code:
            return None
        return self.get_item(self.index_id_code[id])

    def get_children(self, code: str, childrenDao):
        children = []

        parent = self.get_item(code)

        for row in self.parents:
            if row["parent_id"] == parent["id"]:
                id = row["child_id"]
                child = childrenDao.get_item_by_id(id)
                if child is not None:
                    children.append(child)

        return children
