import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";
import { ApiService } from "src/app/service/api.service";
import { DynamicFlatNode, Territory } from "src/app/types/geographic.interface";

/**
 * Database for dynamic data. When expanding a node in the tree, the data source will need to fetch
 * the descendants data from the database.
 */
@Injectable({ providedIn: 'root' })
export class DynamicDatabase {
    constructor(private apiService: ApiService) {

    }

    /** Initial data from database */
    initialData(): Observable<DynamicFlatNode[]> {
        return this.apiService.getDepartments().pipe(
            map((data: Territory[]) => {
                const flatNodes = data.map((territory: Territory) => new DynamicFlatNode(territory, 0, true))
                return flatNodes
            }
            ));
    }

    /** Query department from url */
    getRoot(query: string): Observable<Territory | undefined> {
        return this.apiService.getDepartment(query);
    }

    getChildren(node: DynamicFlatNode): Observable<Territory[]> | undefined {
        switch (node.level) {
            case 0:
                return this.apiService.getDepartmentEpcis(node.item);
            case 1:
                return this.apiService.getEpciCommunes(node.item);
            default:
                return;
        }

    }
}