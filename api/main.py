from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from utils import *
from metadata import *
from DepartementDao import *
from EpciDao import *
from CommuneDao import *
import time

app = FastAPI(openapi_tags=tags_metadata)

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

db = {}

start = time.time()

departmentDao = DepartementDao()
epciDao = EpciDao()
communeDao = CommuneDao()


@app.get("/departements")
def get_departements():
    departements = departmentDao.get_lists()
    return departements


@app.get("/departements/{code}")
def get_departement(code: str):
    departement = departmentDao.get_item(code)
    return departement


@app.get("/departements/{code}/epcis")
def get_departement_epcis(code: str):
    departement_epcis = departmentDao.get_children(code, epciDao)
    return departement_epcis


@app.get("/epcis")
def get_epcis():
    epcis = epciDao.get_lists()
    return epcis


@app.get("/epcis/{code}")
def get_epci(code: str):
    epci = epciDao.get_item(code)
    return epci


@app.get("/epcis/{code}/communes")
def get_epci_communes(code: str):
    epci_communes = epciDao.get_children(code, communeDao)
    return epci_communes


end = time.time()
print("Startup time", (end-start), "sec")
