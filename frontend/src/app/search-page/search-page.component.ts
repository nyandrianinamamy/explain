import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FlatTreeControl } from 'node_modules.nosync/@angular/cdk/tree';
import { Elected } from '../types/elected.interface';
import { DynamicFlatNode } from '../types/geographic.interface';
import { DynamicDataSource } from './dynamic.data-source';
import { DynamicDatabase } from './dynamic.database';

@Component({
  selector: 'app-search-page',
  templateUrl: './search-page.component.html',
  styleUrls: ['./search-page.component.scss']
})
export class SearchPageComponent implements OnInit {
  electeds: Elected[] = [
    {
      firstname: 'Alain',
      name: 'Gallu',
      appearance: 173,
      functions: [
        {
          function: `Président(e) du conseil de département`,
          location: 'Aube'
        },
        {
          function: `Membre du conseil municipal`,
          location: 'Bouilly'
        }
      ]
    },
    {
      firstname: 'Marie-Pierre',
      name: 'Mouton',
      appearance: 46,
      functions: [
        {
          function: `Anc. Vice-président(e) d'EPCI`,
          location: 'CA Troyes Champagne Métropole'
        },
        {
          function: `Anc. Maire`,
          location: 'Burcey-en-Othe'
        }
      ]
    },
    {
      firstname: 'Jean-Michel',
      name: 'Catelinois',
      appearance: 42,
      functions: [
        {
          function: `Vice-président(e) d'EPCI`,
          location: 'CA des portes de Romilly-sur-Seine'
        },
        {
          function: `Anc. Membre du conseil municipal`,
          location: 'Buchères'
        }
      ]
    }
  ];

  dataSource: DynamicDataSource;
  treeControl: FlatTreeControl<DynamicFlatNode>;

  constructor(private route: ActivatedRoute, private router: Router, private database: DynamicDatabase) {
    this.treeControl = new FlatTreeControl<DynamicFlatNode>(this.getLevel, this.isExpandable);
    this.dataSource = new DynamicDataSource(this.treeControl, database);
  }

  ngOnInit() {
    const departmentCode = this.route.snapshot.params.departmentCode ?? null;
    if (!departmentCode) {
      /* Fetch all filters */
      this.dataSource.data = this.database.initialData();
    } else {

      /* Fetch only filters based on department code 
      * Construct FlatNode directly in order to toggle it
      */
      this.database.getRoot(departmentCode).subscribe(territory => {
        if (territory) {
          const flatNode = new DynamicFlatNode(territory, 0, true);
          this.dataSource.data = [flatNode];
          this.dataSource.toggleNode(flatNode, true);
        }
      })
    }
  }

  getLevel = (node: DynamicFlatNode) => node.level;

  isExpandable = (node: DynamicFlatNode) => node.expandable;

}
