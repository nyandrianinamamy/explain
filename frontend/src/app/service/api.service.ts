import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../environments/environment';
import { Territory } from 'src/app/types/geographic.interface';
import { Observable } from 'node_modules.nosync/rxjs';
import { ApiInterfaceService } from './api.service.interface';

@Injectable({
  providedIn: 'root'
})
export class ApiService implements ApiInterfaceService {

  apiUrl: string = environment.apiUrl;

  constructor(private http: HttpClient) { }

  getDepartment(code: string): Observable<Territory | undefined> {
    return this.http.get(`${this.apiUrl}/departements/${code}`) as Observable<Territory>;
  }

  getDepartments(): Observable<Territory[]> {
    return this.http.get(`${this.apiUrl}/departements`) as Observable<Territory[]>;
  }

  getDepartmentEpcis(territory: Territory) {
    return this.http.get(`${this.apiUrl}/departements/${territory.code}/epcis`) as Observable<Territory[]>
  }

  getEpciCommunes(territory: Territory) {
    return this.http.get(`${this.apiUrl}/epcis/${territory.code}/communes`) as Observable<Territory[]>

  }
}
