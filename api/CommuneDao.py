from utils import *
from BaseDao import *


class CommuneDao(BaseDao):

    def __init__(self) -> None:
        super().__init__()
        self.load_territories(Kind.FRCOMM)
        self.load_territory_parent()
