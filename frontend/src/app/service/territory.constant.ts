import { Territory } from "src/app/types/geographic.interface";

export const DEPARTMENTS_MOCK: Territory[] = [
    {
        id: '1',
        name: 'Depa 01',
        code: '01'
    },
    {
        id: '2',
        name: 'Depa 02',
        code: '02'
    }
]

export const EPCIS_MOCK: Territory[] = [
    {
        id: '1',
        name: 'Epci 01',
        code: '001'
    },
    {
        id: '2',
        name: 'Epci 02',
        code: '002'
    }
]

export const COMMUNES_MOCK: Territory[] = [
    {
        id: '3',
        name: 'Commune',
        code: '0001'
    }
]