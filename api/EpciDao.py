from utils import *
from BaseDao import *


class EpciDao(BaseDao):

    def __init__(self) -> None:
        super().__init__()
        self.load_territories(Kind.FREPCI)
        self.load_territory_parent()
