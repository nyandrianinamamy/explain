import csv
from enum import Enum

"""
Reads and returns a csv based on filename.
All files are located in data folder by default.
"""


def read_csv(filename):
    with open("data/" + filename) as csvfile:
        reader = csv.reader(csvfile, delimiter=',')
        next(reader)
        return list(reader)


"""
Enum of different kind of territory
"""


class Kind(Enum):
    FRDEPA = "FRDEPA"
    FREPCI = "FREPCI"
    FRCANT = "FRCANT"
    FRCOMM = "FRCOMM"


"""
Constant of index type
"""
ID = 0
CODE = 1
NAME = 2
KIND = 3
PARENT_ID = 2
CHILD_ID = 1
