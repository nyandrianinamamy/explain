import { Observable } from "node_modules.nosync/rxjs";
import { Territory } from "../types/geographic.interface";

export interface ApiInterfaceService {
    getDepartment(code: string): Observable<Territory | undefined>;
    getDepartments(): Observable<Territory[]>;
    getDepartmentEpcis(territory: Territory): Observable<Territory[]>;
    getEpciCommunes(territory: Territory): Observable<Territory[]>;
}