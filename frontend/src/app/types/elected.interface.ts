import { ElectedFunction } from './function.interface';

export interface Elected {
  name?: string;
  firstname?: string;
  functions?: ElectedFunction[];
  appearance?: number;
}
