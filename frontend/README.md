# Frontend

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 12.1.1.

## First step

Make sure you have the latest stable version of nodejs and angular.

Run `npm install` inside the `frontend` directory.  

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Usage

Default url is `http://localhost:4200/` it will be redirected to `/search` page.  

By default all departments will be queried from the api and shown on the filter.  

To test the filter for only one department navigate to a known department from the db.  

Example `http://localhost:4200/search/01`    

## Mock mode

If the api is not running, there is a mock mode.  

To activate, change the value to `mock: true` in the `src/environments/environments.ts` file.  

Data are fetched from a constant located at `src/app/service/territory.constant.ts`  

## That's all thank you for reading.
## Have a nice day !


