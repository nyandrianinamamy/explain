from utils import *
from BaseDao import *


class DepartementDao(BaseDao):

    def __init__(self) -> None:
        super().__init__()
        self.load_territories(Kind.FRDEPA)
        self.load_territory_parent()
