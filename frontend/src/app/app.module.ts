import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ElectedCardComponent } from './components/elected-card/elected-card.component';
import { ElectedListComponent } from './components/elected-list/elected-list.component';
import { GeographicFilterComponent } from './components/geographic-filter/geographic-filter.component';
import { PaginatorComponent } from './components/paginator/paginator.component';
import { SearchBarComponent } from './components/search-bar/search-bar.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material.module';
import { HttpClientModule } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { ApiService } from './service/api.service';
import { ApiMockService } from './service/api.service.mock';
import { SearchPageComponent } from './search-page/search-page.component';

@NgModule({
  declarations: [
    AppComponent,
    SearchPageComponent,
    ElectedListComponent,
    ToolbarComponent,
    ElectedCardComponent,
    PaginatorComponent,
    SearchBarComponent,
    GeographicFilterComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MaterialModule,
    HttpClientModule,
  ],
  providers: [
    {
      provide: ApiService,
      useClass: environment.mock ? ApiMockService : ApiService
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
