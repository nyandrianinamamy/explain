# API

## How to use

Run 
```
pip install fastapi
pip install uvicorn #or hypercorn
uvicorn main:app --reload
```
## Endpoint 
`http://127.0.0.1:8000`

## Docs and tests
`http://127.0.0.1:8000/docs`

