import { Injectable } from '@angular/core';
import { Territory } from 'src/app/types/geographic.interface';
import { Observable, of } from 'rxjs';
import { COMMUNES_MOCK, DEPARTMENTS_MOCK, EPCIS_MOCK } from './territory.constant';
import { ApiInterfaceService } from './api.service.interface';

@Injectable()
export class ApiMockService implements ApiInterfaceService {

    getDepartment(code: string): Observable<Territory | undefined> {
        return of(DEPARTMENTS_MOCK.find(territory => territory.code == code));
    }

    getDepartments(): Observable<Territory[]> {
        return of(DEPARTMENTS_MOCK)
    }

    getDepartmentEpcis(territory: Territory) {
        return of(EPCIS_MOCK)
    }

    getEpciCommunes(territory: Territory) {
        return of(COMMUNES_MOCK)
    }
}
